public class TreeKanga
{
	private String name;
	private int size;
	private char gender;
	private String color;
	
	
	//Constructor part
	public TreeKanga(String name, int size, char gender, String color)
	{
		this.name = name;
		this.size = size;
		this.gender = gender;
		this.color = color;
	}
	
	//Creating the getters and setters for the fields
	//All the getters
	public String getName()
	{
		return this.name;
	}
	
	public int getSize()
	{
		return this.size;
	}
	
	public char getGender()
	{
		return this.gender;
	}
	
	public String getColor()
	{
		return this.color;
	}
	
	//All the setters
	public void setName(String newName)
	{
		this.name = newName;
	}
	
	public void setSize(int newSize)
	{
		this.size = newSize;
	}
	
	/* Gender set method deleted 
	public void setGender(char newGender)
	{
		this.gender = newGender;
	}*/
	
	public void setColor(String newColor)
	{
		this.color = newColor;
	}
	
	
	
	public void CarryBaby() 
	{
	if (this.gender =='M')
		{
		System.out.println(name + " is carrying a baby in his marsupium");
		}
	else
		{
		System.out.println(name + " is carrying a baby in her marsupium");
		}
	}	
	public void IsFertile() 
	{
	if (this.gender =='M')
		{
		System.out.println(name + " is fertile and taking advantage of his "+ color +" fur to attract females");
		}
	else
		{
		System.out.println(name + " is fertile and taking advantage of her "+ color +" fur to attract males");
		}
	}
	//LAB 04 starts here. 3rd instance method. It will take the high of the Kangaroo and then decide how tall they can climb based on their high.
	
	public void ClimbTree() 
	{
		if (isValid(size))
		{
			HelpClimbTree();
		}
		else
		{
			System.out.println("That's too big to be a Tree Kangaroo!");
		}
			
		
	}
	
	private void HelpClimbTree()
	{
		if (this.size > 85) //max size should be 90 cm
			{
			System.out.println(name + " is climbing a tree 3 meters tall");
			}
		else if (this.size >75)
			{
			System.out.println(name + " is climbing a tree 2 meters tall");
			}
		else
			{
			System.out.println(name + " is climbing a tree 1 meter tall");
			}
	}
	
	private boolean isValid(int size) 
	{
		boolean isValidValue = false;
		if (size <= 90)
		{
			isValidValue = true;
		}
		return isValidValue;
	
	}
}