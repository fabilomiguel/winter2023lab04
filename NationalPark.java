import java.util.Scanner;
public class NationalPark{
	
	public static final Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		TreeKanga[] mob = new TreeKanga[3]; //A group of Kangaroos is called "mob"
		
		//loop starts
		for (int i=0; i < mob.length; i++)
		{
			
			System.out.println("Please enter the name of your Tree Kangaroo");			
			String name = scan.next();
			System.out.println("Please enter the size (in cm) of your Tree Kangaroo");
			int size = scan.nextInt();
			System.out.println("Please enter the gender (M or F) of your Tree Kangaroo"); //Assuming the user will only use the char in capital letters
			char gender = scan.next().charAt(0);
			System.out.println("Please enter the color of your Tree Kangaroo");
			String color = scan.next();
			System.out.println();
			
			mob[i] = new TreeKanga(name, size, gender, color);
			
		}
		
		//Part 3 step 4 - Setter for the last tree Kangaroo
		
		//Printing before calling the setters
		System.out.println("The last Kangaroo entered was:  "+mob[2].getName() +", " + mob[2].getSize() + ", " + mob[2].getGender() + ", " + mob[2].getColor());
		
		System.out.println("Please enter the name of your Tree Kangaroo");			
		mob[2].setName(scan.next());
		System.out.println("Please enter the size (in cm) of your Tree Kangaroo");
		mob[2].setSize(scan.nextInt());
		//Gender cant be changed by user, so it's not called.
		System.out.println("Please enter the color of your Tree Kangaroo");
		mob[2].setColor(scan.next());
		System.out.println();
		
		//Printing after calling the setters
		System.out.println("The last Kangaroo entered was:  "+mob[2].getName() +", " + mob[2].getSize() + ", " + mob[2].getGender() + ", " + mob[2].getColor());
		
		mob[0].IsFertile();
		mob[0].ClimbTree();
		mob[0].CarryBaby();
		
		//LAB 04 Starts here
		
		mob[1].ClimbTree();
	}
	
}